package warudachi;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;

public class EntityAIIgnorePlayer extends EntityAIBase {
    private EntityCreature theEntity;
    
    public EntityAIIgnorePlayer(EntityCreature par1EntityCreature)
    {
        this.theEntity = par1EntityCreature;
        this.setMutexBits(1);
    }

	@Override
	public boolean shouldExecute() {
//		System.out.println("Warudachi: entity = " + theEntity.toString() +  ",target = " + theEntity.getAttackTarget());
		if (theEntity.getAttackTarget() != null) {
//			System.out.println("Warudachi: target = " + theEntity.getAttackTarget().toString());
			if (theEntity.getAttackTarget() instanceof EntityPlayer) {
				EntityPlayer target = (EntityPlayer)theEntity.getAttackTarget();
//				System.out.println("Warudachi: player = " + theEntity.getAttackTarget().toString());
				if (target.inventory.armorInventory[2] != null) {
//					System.out.println("Warudachi: target inventory2 = " + target.inventory.armorInventory[2]);
					if (target.inventory.armorInventory[2].getItem() instanceof WarudachiArmor) {
//						System.out.println("Warudachi: setAttackTarget=null");       
						return true;
					}
				}
			}
		}
		return false;
	}
}
