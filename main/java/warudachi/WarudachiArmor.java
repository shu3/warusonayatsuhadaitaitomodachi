package warudachi;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.monster.EntityWitch;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class WarudachiArmor extends ItemArmor {
	private static int LIMIT_SEC = 3;

	public WarudachiArmor(ArmorMaterial material, int id, int placement) {
		super(material, id, placement);
		
		this.setTextureName(WarudachiMod.MODID + ":bling_bling");
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type){
		if (stack.getItem() == WarudachiMod.armor) {
			return WarudachiMod.MODID + ":textures/models/armor/bling_bling_layer.png";
		}
		return null;
	}
	
	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack){
		if(itemStack.getItem() == WarudachiMod.armor){
			if (world.isRemote == true) {
				return;
			}
			
			injectAI(world, player);
		}
	}

    private List<EntityCreature> nearMobs(World world, EntityPlayer player) {
        AxisAlignedBB range = AxisAlignedBB.getBoundingBox(player.posX-16, player.posY-4, player.posZ-16, player.posX+16, player.posY+4, player.posZ+16);
        List list = world.getEntitiesWithinAABB(EntityCreature.class, range);
        return list;
    }
    
    private void untargetPlayer(World world, EntityPlayer player) {
        List<EntityCreature> list = nearMobs(world, player);
        for (EntityCreature e : list) {
        	System.out.println("Warudachi: untarget MOB = " + e.toString());
        	e.setTarget(null);
        }
    }
    
    private void injectAI(World world, EntityPlayer player) {
        List<EntityCreature> list = nearMobs(world, player);
        for (EntityCreature e : list) {
        	// replaced of isAiEnabled()
    		if (e instanceof EntityWitch || e instanceof EntityWither ||
        			e instanceof EntityIronGolem || e instanceof EntitySkeleton ||
        			e instanceof EntitySnowman || e instanceof EntityZombie ||
        			e instanceof EntityCreeper) {
    			// for new AI
    			EntityAITaskEntry ai;
    			if (!isAIInjected(e)) {
    				System.out.println("Warudachi2: injected MOB = " + e.toString());
    				e.tasks.addTask(0, new EntityAIIgnorePlayer(e));
    			}
    		}
    		else {
    			// for old AI
    			if (e.getEntityToAttack() != null && e.getEntityToAttack() instanceof EntityPlayer) {
    				e.setTarget(null);
    			}
    		}
        }
    }
    
    private boolean isAIInjected(EntityCreature e) {
        List<EntityAITaskEntry> entries = e.tasks.taskEntries;
        for (EntityAITaskEntry entry : entries) {
                if (entry.action instanceof EntityAIIgnorePlayer) {
                        return true;
                }
        }
        return false;
    }
}