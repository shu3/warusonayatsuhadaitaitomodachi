package warudachi;

import net.minecraft.block.Block;
import net.minecraft.block.BlockNote;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = WarudachiMod.MODID, version = WarudachiMod.VERSION)
public class WarudachiMod {
    public static final String MODID = "warudachi";
    public static final String VERSION = "1.0";
    public static int armorId;
    public static Item armor;

    @EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
    	armor = new WarudachiArmor(ItemArmor.ArmorMaterial.GOLD, armorId, 1)
			.setUnlocalizedName("bling_bling");
    	GameRegistry.registerItem(armor, "bling_bling");
    	GameRegistry.addRecipe(new ItemStack(this.armor, 1),
    			"G G", "G G", " S ",
    			'G', Items.gold_ingot,
    			'S', new ItemStack(Items.skull, 1, 1));
	}
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
		// some example code
    }
}
